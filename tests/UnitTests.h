/*
 * UnitTests.h
 *
 *  Created on: 20 Feb 2019
 *      Author: SanWork
 */

#ifndef UNITTESTS_H_
#define UNITTESTS_H_

#include "Storyboard.h"

class TpCTestStoryboard
{
public:
    void runAllTests();

private:
    void testInsertMultiple();

    void testInsertNote(const TpCNote& rNote);
    void testDeleteNote(const TpCNote& rNote);

    void testTitleSearchInsertedNote(const TpCNote& rNote);
    void testTitleSearchDeletedNote(const TpCNote& rNote);

    void testTextSearchInsertedNote(const TpCNote& rNote);
    void testTextSearchDeletedNote(const TpCNote& rNote);

    void testTagsSearchInsertedNote(const TpCNote& rNote);
    void testTagsSearchDeletedNote(const TpCNote& rNote);
private:
    TpCStoryboard   m_Storyboard;
};


#endif /* UNITTESTS_H_ */
