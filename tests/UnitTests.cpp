/*
 * UnitTests.cpp
 *
 *  Created on: 20 Feb 2019
 *      Author: SanWork
 */

#include "UnitTests.h"
#include "Note.h"

void
TpCTestStoryboard::runAllTests()
{
    testInsertMultiple();
}


void
TpCTestStoryboard::testInsertMultiple()
{
    std::cout << std::endl << std::endl << "Testing multiple inserts " << std::endl << "********" << std::endl;
    TpTTags TagsFirstNote;
    TagsFirstNote.insert("unit test");
    TagsFirstNote.insert("testing");
    TpCNote FirstNote ("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       TagsFirstNote);
    TpTTags TagsSecondNote;
    TagsFirstNote.insert("unit test");
    TagsFirstNote.insert("testing");
    TagsFirstNote.insert("traceplayer");
    TpCNote SecondNote ("Test Traceplayer extension",
                       "Implement a unit test for the class Traceplayer extension of the spark core framework.",
                       TagsFirstNote);

    testInsertNote(FirstNote);
    testInsertNote(SecondNote);
    testTitleSearchInsertedNote(FirstNote);
    testTextSearchInsertedNote(FirstNote);
    testTagsSearchInsertedNote(FirstNote);

    testTitleSearchInsertedNote(SecondNote);
    testTextSearchInsertedNote(SecondNote);
    testTagsSearchInsertedNote(SecondNote);

    testDeleteNote(FirstNote);
    testTitleSearchDeletedNote(FirstNote);
    testTextSearchDeletedNote(FirstNote);
    testTagsSearchDeletedNote(FirstNote);
}

void
TpCTestStoryboard::testInsertNote(const TpCNote& rNote)
{
    UInt32 NotesSize = 0;
    UInt32 PrevNotesSize  = 0;
    std::cout << "*** TESTING --> Insert:" << std::endl;
    PrevNotesSize = m_Storyboard.getNotesSize();
    m_Storyboard.addNote(rNote);
    NotesSize = m_Storyboard.getNotesSize();
    if (NotesSize == PrevNotesSize+1)
    {
    		std::cout << "PASSED: Insert one element" << std::endl;
    }
    else
    {
    		std::cout << "ERROR: Insert one element failed" << std::endl;
        exit(-1);
    }
}
void
TpCTestStoryboard::testDeleteNote(const TpCNote& rNote)
{
    UInt32 NotesSize = 0;
    UInt32 PrevNotesSize  = 0;
    std::cout << "*** TESTING --> Delete:" << std::endl;
    PrevNotesSize = m_Storyboard.getNotesSize();
    m_Storyboard.deleteNote(rNote);
    NotesSize = m_Storyboard.getNotesSize();
    if (NotesSize == PrevNotesSize-1)
    {
        std::cout << "PASSED: Delete one element" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Delete one element failed" << std::endl;
        exit(-2);
    }
}

void
TpCTestStoryboard::testTitleSearchInsertedNote(const TpCNote& rNote)
{
    TpCNote SearchedNote;
    std::cout << "*** TESTING --> title search inserted:" << std::endl;
    bool bFoundElement = m_Storyboard.searchByTitle(rNote.Title, SearchedNote);
    if (bFoundElement == true)
    {
        std::cout << "PASSED: Search inserted element" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Search inserted failed" << std::endl;
        exit(-3);
    }
}

void
TpCTestStoryboard::testTitleSearchDeletedNote(const TpCNote& rNote)
{
    TpCNote SearchedNote;
    std::cout << "*** TESTING --> title search deleted:" << std::endl;
    bool bFoundElement = m_Storyboard.searchByTitle(rNote.Title, SearchedNote);
    if (bFoundElement == false)
    {
        std::cout << "PASSED: Search deleted element - not found" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Search deleted failed - found" << std::endl;
        exit(-4);
    }
}


void
TpCTestStoryboard::testTextSearchInsertedNote(const TpCNote& rNote)
{
    TpCNote SearchedNote;
    std::cout << "*** TESTING --> text search inserted:" << std::endl;
    bool bFoundElement = m_Storyboard.searchByText(rNote.Text, SearchedNote);
    if (bFoundElement == true)
    {
        std::cout << "PASSED: Search inserted element" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Search inserted failed" << std::endl;
        exit(-5);
    }
}

void
TpCTestStoryboard::testTextSearchDeletedNote(const TpCNote& rNote)
{
    TpCNote SearchedNote;
    std::cout << "*** TESTING --> text search deleted:" << std::endl;
    bool bFoundElement = m_Storyboard.searchByText(rNote.Text, SearchedNote);
    if (bFoundElement == false)
    {
        std::cout << "PASSED: Search deleted element - not found" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Search deleted failed - found" << std::endl;
        exit(-6);
    }
}


void
TpCTestStoryboard::testTagsSearchInsertedNote(const TpCNote& rNote)
{
    TpCNote SearchedNote;
    std::cout << "*** TESTING --> tags search inserted:" << std::endl;
    bool bFoundElement = m_Storyboard.searchByTag((const TpTTags&)rNote.Tags, SearchedNote);
    if (bFoundElement == true)
    {
        std::cout << "PASSED: Search inserted element" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Search inserted failed" << std::endl;
        exit(-7);
    }
}

void
TpCTestStoryboard::testTagsSearchDeletedNote(const TpCNote& rNote)
{
    TpCNote SearchedNote;
    std::cout << "*** TESTING --> tags search deleted:" << std::endl;
    bool bFoundElement = m_Storyboard.searchByTag((const TpTTags&)rNote.Tags, SearchedNote);
    if (bFoundElement == false)
    {
        std::cout << "PASSED: Search deleted element - not found" << std::endl;
    }
    else
    {
        std::cout << "ERROR: Search deleted failed - found" << std::endl;
        exit(-8);
    }
}
