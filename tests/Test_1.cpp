//============================================================================
// Name        : Interview1.cpp
// Author      : Sandeep Korrapati
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "UnitTests.h"

//using namespace std;


/***
 * Assumptions:
 * Title and Text of different Notes are unique.
 */

int main() {
	std::cout << "!!! Welcome to the Storyboard !!!" << std::endl;

    TpCTestStoryboard TestObj;
    TestObj.runAllTests();
    return 0;
}
