# HIL: No spaces or comments after otherwise it captures them!
EXECUTABLE := Storyboard

# Determine the platform
UNAME_S := $(shell uname -s)

# CC
ifeq ($(UNAME_S),Darwin)
  CC := clang++ -arch x86_64
else
  CC := g++
endif

# Folders
SRCDIR := src
BUILDDIR := build
TESTSBUILDDIR := build/tests
TESTSSRCDIR := tests
TARGETDIR := bin

# Targets
TARGET := $(TARGETDIR)/$(EXECUTABLE)

## Final Paths
#INSTALLBINDIR := /usr/local/bin

# Code Lists
SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name '*.$(SRCEXT)')
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

# Folder Lists
# Note: Intentionally excludes the root of the include folder so the lists are clean
INCDIRS := $(shell find include/**/* -name '*.h' -exec dirname {} \; 2> /dev/null | sort | uniq)
INCLIST := $(patsubst include/%,-I include/%,$(INCDIRS))
BUILDLIST := $(patsubst include/%,$(BUILDDIR)/%,$(INCDIRS))

#Note: Add root build directory to compile source files in the 
BUILDLIST += $(BUILDDIR)


# Shared Compiler Flags
CFLAGS := -c -fPIC
INC := -I include $(INCLIST) -I /usr/local/include
LIB := #-L /usr/local/lib

# Platform Specific Compiler Flags
ifeq ($(UNAME_S),Linux)
    CFLAGS += -std=gnu++14 -O2 # -fPIC

    # PostgreSQL Special
    PG_VER := 9.3
    INC += -I /usr/pgsql-$(PG_VER)/include
    LIB += -L /usr/pgsql-$(PG_VER)/lib
else
  CFLAGS += -std=c++14 -stdlib=libc++ -O2
endif

all: $(TARGET) tests

$(TARGET): $(OBJECTS)
	@mkdir -p $(TARGETDIR)
	@echo "Linking..."
	@echo "  Linking $(TARGET) "; $(CC) -shared $^ -o $(TARGET) $(LIB)


$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDLIST)
	@echo "Compiling $<..."; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

tests: test_1

$(TESTSBUILDDIR)/%.o: $(TESTSSRCDIR)/%.$(SRCEXT)
	@mkdir -p $(TESTSBUILDDIR)
	@echo "Compiling $<..."; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

test_1: $(TESTSBUILDDIR)/UnitTests.o $(TESTSBUILDDIR)/Test_1.o $(TARGET)
	@echo "Linking..."
	@echo "  Linking $@ "; $(CC) $^ -o $(TARGETDIR)/$@
	

clean:
	@echo "Cleaning $(TARGET)..."; $(RM) -r $(BUILDDIR) $(TARGETDIR)

install:
	@echo "Installing $(EXECUTABLE)..."; cp $(TARGET) $(INSTALLBINDIR)
  
distclean:
	@echo "Removing $(EXECUTABLE)"; rm $(INSTALLBINDIR)/$(EXECUTABLE)

.PHONY: clean