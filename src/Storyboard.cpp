/*
 * Storyboard.cpp
 *
 *  Created on: 20 Feb 2019
 *      Author: SanWork
 */

#include "Storyboard.h"
#include <algorithm>

TpCStoryboard::TpCStoryboard()
{
    m_Notes.reserve(100);
    m_TitleNoteMap.reserve(100);
    m_TextNoteMap.reserve(100);
}

TpCStoryboard::~TpCStoryboard()
{
    m_TitleNoteMap.clear();
    m_TextNoteMap.clear();
}

UInt32
TpCStoryboard::getNotesSize() const
{
    return (UInt32)m_Notes.size();
}

void
TpCStoryboard::addNote(const TpCNote& rNoteEntry)
{
    TpTNoteSptr NewNote = std::make_shared<TpCNote>(rNoteEntry);
    m_TitleNoteMap[NewNote->Title]  = NewNote;
    m_TextNoteMap[NewNote->Text]    = NewNote;
    m_Notes.push_back(NewNote);
    return;
}


void
TpCStoryboard::deleteNote(const TpCNote& rNoteEntry)
{
    auto Iter = m_TitleNoteMap.find(rNoteEntry.Title);
    if (Iter != m_TitleNoteMap.end())
    {
        TpTNoteSptr pNote = Iter->second;
        if (rNoteEntry.Text.compare(pNote->Text) == 0)
        {
            {
                // Delete Note
                auto NoteIter = find(m_Notes.begin(), m_Notes.end(), pNote);
                if (NoteIter != m_Notes.end())
                {
                    m_Notes.erase(NoteIter);
                }
            }
            m_TextNoteMap.erase(rNoteEntry.Text);
            m_TitleNoteMap.erase(Iter);
        }
    }
    return;
}

bool
TpCStoryboard::searchByTitle(const std::string SearchTitle, TpCNote& rFoundNoteCopy) const
{
    bool bElementFound = false;
    auto Iter = m_TitleNoteMap.find(SearchTitle);
    if (Iter != m_TitleNoteMap.end())
    {
        TpTNoteSptr pNote = Iter->second;
        rFoundNoteCopy = *pNote.get();
        bElementFound = true;
    }
    return bElementFound;
}


bool
TpCStoryboard::searchByText(const std::string SearchText, TpCNote& rFoundNoteCopy) const
{
    bool bElementFound = false;
    auto Iter = m_TextNoteMap.find(SearchText);
    if (Iter != m_TextNoteMap.end())
    {
        TpTNoteSptr pNote = Iter->second;
        rFoundNoteCopy = *pNote.get();
        bElementFound = true;
    }
    return bElementFound;
}

bool
TpCStoryboard::compareTags(const TpTTags& rSearchTags, const TpTTags& rNoteTags) const
{
    bool bTagsEqual = false;
    if (rNoteTags.size() == rSearchTags.size())
    {
        UInt32 nFoundTags = 0;
        for (const auto& Tag: rSearchTags)
        {
            if (rNoteTags.find(Tag) != rNoteTags.end())
            {
                ++nFoundTags;
            }
            else
            {
                break;
            }
        }
        if (nFoundTags == rSearchTags.size())
        {
            bTagsEqual = true;
        }
    }
    return bTagsEqual;
}

bool
TpCStoryboard::searchByTag(const TpTTags& rSearchTags, TpCNote& rFoundNoteCopy) const
{
    bool bElementFound = false;

    if (rSearchTags.size() == 0)
    {
        return bElementFound;
    }
    for (const auto& pNote: m_Notes)
    {
        if (compareTags(rSearchTags, pNote->Tags) == true)
        {
            rFoundNoteCopy = *pNote.get();
            bElementFound = true;
        }
    }
    return bElementFound;
}
