/*
 * Note.h
 *
 *  Created on: 20 Feb 2019
 *      Author: SanWork
 */

#ifndef NOTE_H_
#define NOTE_H_

#include "DataTypes.h"

class TpCNote
{
public:
    std::string     Title;
    std::string     Text;
    TpTTags         Tags;

    TpCNote() :
        Title(""),
        Text("")
    {

    }
    TpCNote(std::string _Title, std::string _Text, TpTTags _Tags) :
        Title(_Title),
        Text(_Text),
        Tags(_Tags)
    {
    }


    TpCNote(const TpCNote& Src) :
        Title(Src.Title),
        Text(Src.Text),
        Tags(Src.Tags)
    {
    }
};



#endif /* NOTE_H_ */
