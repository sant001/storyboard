/*
 * Storyboard.h
 *
 *  Created on: 20 Feb 2019
 *      Author: SanWork
 */

#ifndef STORYBOARD_H_
#define STORYBOARD_H_

#include "DataTypes.h"
#include "Note.h"


class TpCStoryboard
{
public:
    TpCStoryboard();
    ~TpCStoryboard();

    void addNote(const TpCNote& rNoteEntry);
    void deleteNote(const TpCNote& rNoteEntry);


    bool searchByTitle(const std::string SearchTitle, TpCNote& rFoundNoteCopy) const;
    bool searchByText(const std::string SearchText, TpCNote& rFoundNoteCopy) const;
    bool searchByTag(const TpTTags& rSearchTags, TpCNote& rFoundNoteCopy) const;

    /* UnitTest Helper functions */
    UInt32 getNotesSize() const;
private:
    bool compareTags(const TpTTags& rSearchTags, const TpTTags& rNoteTags) const;
    TpTNotesList            m_Notes;
    TpTTextNoteMap          m_TitleNoteMap;
    TpTTextNoteMap          m_TextNoteMap;
};


#endif /* STORYBOARD_H_ */
