/*
 * DataTypes.h
 *
 *  Created on: 20 Feb 2019
 *      Author: SanWork
 */

#ifndef DATATYPES_H_
#define DATATYPES_H_
#include <iostream>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <string>

//using namespace std;

typedef uint32_t                UInt32;
typedef int32_t                 Int32;

class TpCNote;
typedef std::shared_ptr<TpCNote>                        TpTNoteSptr;
typedef std::unordered_map<std::string, TpTNoteSptr>    TpTTextNoteMap;
typedef std::unordered_set<std::string>                 TpTTags;
typedef std::vector<TpTNoteSptr>                        TpTNotesList;


#endif /* DATATYPES_H_ */
